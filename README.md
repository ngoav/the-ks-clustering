ORNL: Sept 25th, 2023.
This package contains two ipython scripts for two "good" and "bad" models, 
in which we show how the KS clustering algorithm works

This package also contains three similar ipython scripts applied for three protein systems
using the KS clustering algorithm.

There are also notes in each notebook.

Please send an email to ngoav :AT: ornl.gov to have two large Toymodel pickles

Please cite [.]: Van A. Ngo, Yen Ting Li, Danny Perez. "Improving Estimation of the Koopman Operator with Kolmogorov-Smirnov Indicator functions." https://doi.org/10.1021/acs.jctc.3c00632
