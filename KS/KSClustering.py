#### Please cite [.]: Van A. Ngo, Yen Ting Li, Danny Perez. 
#### "Improving Estimatioon of Koopman Operators with Kolmogorov-Smirnov Indicator functions." 
#### Copyright: ORNL, LANL.
from scipy.stats import ks_2samp
import numpy as np

from scipy.cluster.hierarchy import dendrogram, linkage
from scipy.cluster.hierarchy import fcluster
import sklearn.cluster


# build 2D KS matrix
def KS_measure(x,block=100):
    # block is delta_t used in the paper
    d1=np.reshape(x, x.size)
    print('using block =',block)
    l=len(d1)
    nb=int(l/block)
    KS=np.zeros((nb,nb))
    for i in range(nb):
        ki1=i*block
        ki2=(i+1)*block
        dai=d1[ki1:ki2]
        for j in range(i,nb):
            kj1=j*block
            kj2=(j+1)*block
            daj=d1[kj1:kj2]
            D,p=ks_2samp(dai,daj)
            KS[i,j]=D
            KS[j,i]=D
    return KS
class clustering:
    #set the critical distance from the KS criterion
    def __init__(self,KS,k=2,block=100):
        # block is delta_t used in the paper
        def kdtd(n):
            return 1.6*np.sqrt(2*n/n**2)*k
        print("Danny's Threshold: ",kdtd(block))
        cls=sklearn.cluster.AgglomerativeClustering(n_clusters=None,  affinity='precomputed', memory=None, connectivity=None, compute_full_tree='auto', linkage='average', distance_threshold=kdtd(block))
        #cls.fit(KS)
        self.clustertime=cls.fit(KS).labels_
        
        tg2=[]
        av=np.average(self.clustertime)
        nstate=np.unique(self.clustertime)
        self.Single_cluster_KS_surrogate=[]
        for st  in nstate:
            tg=[]
            print('indicator function ', st)
            for cl in self.clustertime:
                
                if cl == st:
                    tg=np.concatenate((tg,np.full(block,1)))
                else:
                    tg=np.concatenate((tg,np.full(block,0)))
            tg2.append(tg)    
        self.cluster_KS_surrogate=tg2
        for cl in self.clustertime:
            self.Single_cluster_KS_surrogate=np.concatenate((self.Single_cluster_KS_surrogate,np.full(block,cl-av)))

            
##
class clustering2:
    #set a desired number of clusters (k); by default we use this algorithm
    def __init__(self,KS,k=2,block=100):
        # block is delta_t used in the paper
        #def kdtd(n):
        #    return 1.6*np.sqrt(2*n/n**2)*k
        print("Using number of ",k,'clusters')
        cls=sklearn.cluster.AgglomerativeClustering(n_clusters=k,  affinity='precomputed', memory=None, connectivity=None, compute_full_tree='auto', linkage='average')
        #cls.fit(KS)
        self.clustertime=cls.fit(KS).labels_
        
        tg2=[]
        av=np.average(self.clustertime)
        nstate=np.unique(self.clustertime)
        self.Single_cluster_KS_surrogate=[]
        for st  in nstate:
            tg=[]
            print('indicator function ', st)
            for cl in self.clustertime:
                
                if cl == st:
                    tg=np.concatenate((tg,np.full(block,1)))
                else:
                    tg=np.concatenate((tg,np.full(block,0)))
            tg2.append(tg)    
        self.cluster_KS_surrogate=tg2
        for cl in self.clustertime:
            self.Single_cluster_KS_surrogate=np.concatenate((self.Single_cluster_KS_surrogate,np.full(block,cl-av)))

def get_timescale(vamp_object,kmeans=2,lag=20):
    #This is to compute timescale from k-means clustering 
    #and Markov State Modeling (see Vijay Pande's and Frank Noe's)
    import pyemma
    import pyemma.coordinates as coor
    vamp_output=vamp_object.get_output()
    vamp_clustering=coor.cluster_kmeans(vamp_output,k=kmeans)
    dtrajs=vamp_clustering.dtrajs
    time=np.arange(len(dtrajs[0]))
    fig,ax=plt.subplots(1,1,figsize=(8,6))
    print(dtrajs[0])
    ax.plot(time,dtrajs[0])
    ax.set_ylabel('Discretized Trajectory')
    ax.set_xlabel('Time')
    plt.show()
    vamp_msm=pyemma.msm.estimate_markov_model(dtrajs,lag=lag)
    timescale=vamp_msm.timescales()[0]
    print('stationary measure',vamp_msm.stationary_distribution)
    return timescale

def compute_timescale_(tau1,tau2):
    p1=1/tau1; p2=1/tau2
    return -1/np.log(1-p1-p2)

def compute_timescale_0(tau1,tau2):
    
    return tau1*tau2/(tau1+tau2)

def ntransitions(count):
    # count is a number of times that an indicator function changes its value from 0->1 or 1->0
    n=int(count/2)
    if count % 2 == 0:
        return n
    else:
        return n+1
    

def compute_transition(x,dt=1):
    # x must be an indicator  function of {0 and 1}
    if np.average(x) == 0 or np.average(x) == 1:
        print("ERROR: input is not indicator function")
        return 0,0
    else:
    	prob=np.sum(x)
    	#count number  of transitions
    	count=-1; # exclude the first count --> mark changes of indicator function
    	ix0=-1
    	for ix in x:
            if ix != ix0:
            	count+=1
            ix0=ix
    	n=ntransitions(count)
    	fwtime=prob/n*dt
    	bwtime=(len(x)-prob)/n*dt
    	print('found ',n,' transition events')
    	return fwtime,bwtime
        
# Compute global KS matrix

def compute_global_matrix(vamp_data,dim=10,block=2100):

    local_matrix=[]
    for d in range(dim):
        temp=KS_measure(np.vstack(vamp_data)[:,d],block=block)
        local_matrix.append(temp)
        global_matrix=temp
    print(np.shape(temp))
    for i in range(np.shape(temp)[0]):
        for j in range(np.shape(temp)[1]):
            max_val=-1000
            for local in local_matrix:
                if max_val < local[i][j]:
                    max_val = local[i][j]
            global_matrix[i][j]=max_val
    return global_matrix

def Add_indicatorF(global_matrix,x,dim=10,block=2100,dt=1.0):
    # now do the clustering with this global_maxtrix to produce dim indicator functions:
    # dim = 1, there is an uniform indicator function of value 1, not contributing any thing to timescale
    # so when dim = 1, the timescale is the same as the orignal
    temp_clustering=clustering2(global_matrix,k=dim,block=block)
    KS_global=[]
    l=len(temp_clustering.cluster_KS_surrogate[0])
    for d in range(dim):
        tg = temp_clustering.cluster_KS_surrogate[d]
        KS_global.append(tg)
        print('transition times',compute_transition(tg,dt))
    combined=np.hstack((np.vstack(x)[:l,:],np.asarray(KS_global).T[:,:]))
    print(np.shape(combined),'should be (Trajectory_length, dimension)')
    return combined
    
